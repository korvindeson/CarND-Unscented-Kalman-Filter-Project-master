﻿#include "ukf.h"
#include "Eigen/Dense"
#include <iostream>

using namespace std;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::vector;

/**
 * Initializes Unscented Kalman filter
 */
UKF::UKF() {
  // if this is false, laser measurements will be ignored (except during init)
  use_laser_ = true;

  // if this is false, radar measurements will be ignored (except during init)
  use_radar_ = true;
  
  //set state dimensions
  n_x_ = 5;
  n_y_ = 2 * n_x_ + 1;
  n_aug_ = 7;
  n_a_ = 2 * n_aug_ + 1;
  //n_z_ = 3;

  // initial state vector
  x_ = VectorXd(n_x_);

  // initial covariance matrix
  P_ = MatrixXd(n_x_, n_x_);
  P_.fill(0.0);
  // fill diagonal with 1
  for (int i = 0; i < n_x_; i++) {
	  P_.diagonal()[i] = 1.0;
  }
  
  // Process noise standard deviation longitudinal acceleration in m/s^2
  std_a_ = 1.5;

  // Process noise standard deviation yaw acceleration in rad/s^2
  std_yawdd_ = 0.5;

  // Laser measurement noise standard deviation position1 in m
  std_laspx_ = 0.15;

  // Laser measurement noise standard deviation position2 in m
  std_laspy_ = 0.15;

  // Radar measurement noise standard deviation radius in m
  std_radr_ = 0.3;

  // Radar measurement noise standard deviation angle in rad
  std_radphi_ = 0.03;

  // Radar measurement noise standard deviation radius change in m/s
  std_radrd_ = 0.3;

  // set init flag
  is_initialized_ = false;
  
  //define spreading parameter
  lambda_ = 3 - n_aug_;

  // set weights
  weights_ = VectorXd(2 * n_aug_ + 1);
  weights_(0) = lambda_ / (lambda_ + n_aug_);

  double weight = 0.5 / (n_aug_ + lambda_);
  for (int i = 1; i < 2 * n_aug_ + 1; i++) {
	  weights_(i) = weight;
  }

  
  R_radar_ = MatrixXd(3, 3);
  R_radar_ << std_radr_ * std_radr_,  0, 0,
			  0,std_radphi_*std_radphi_, 0,
			  0, 0, std_radrd_*std_radrd_;

  R_laser_ = MatrixXd(2, 2);
  R_laser_ << std_laspx_*std_laspx_, 0,
			  0, std_laspy_*std_laspy_;
  //create vector for predicted state
  Xsig_pred_ = MatrixXd(n_x_, n_a_);

  //Zsig = MatrixXd(n_z_, 2 * n_aug_ + 1);
  //Xsig_aug = MatrixXd(n_aug_, 2 * n_aug_ + 1);
  
  //Xsig_pred = MatrixXd(n_x_, 2 * n_aug_ + 1);
}

UKF::~UKF() {}

/**
 * @param {MeasurementPackage} meas_package The latest measurement data of
 * either radar or laser.
 */
void UKF::ProcessMeasurement(MeasurementPackage meas_package) {
  
	cout << "ProcessMeasurement\n";

	// ***** Initialization ***** //
	if (!is_initialized_) {


		///// Do we need to init x_? ////////////

		if (meas_package.sensor_type_ == MeasurementPackage::RADAR) {
			// x_ = rho * cos(phi), rho * sin(phi), 4, rho' * cos(phi), rho' * sin(phi)
			//x_ << meas_package.raw_measurements_[0] * cos(meas_package.raw_measurements_[1]),
			//	  meas_package.raw_measurements_[0] * sin(meas_package.raw_measurements_[1]), 
			//	  4,
			//	  meas_package.raw_measurements_[2] * cos(meas_package.raw_measurements_[1]),
			//	  meas_package.raw_measurements_[2] * cos(meas_package.raw_measurements_[1]);
			
			// x_ = rho * cos(phi), rho * sin(phi), sqrt(vx * vx + vy * vy), 0, 0
			x_ << meas_package.raw_measurements_[0] * cos(meas_package.raw_measurements_[1]),
				  meas_package.raw_measurements_[0] * sin(meas_package.raw_measurements_[1]),
				  meas_package.raw_measurements_[2],
				  0,
				  0;
		}
		else {
			// 2 dimensions for laser
			x_ << meas_package.raw_measurements_[0], meas_package.raw_measurements_[1], 0, 0, 0;
			// lower values do not make sence to use
			if (fabs(x_(0)) < 0.001 && fabs(x_(1)) < 0.001) {
				x_(0) = 0.001;
				x_(1) = 0.001;
			}
		}

		//init time
		time_us_ = meas_package.timestamp_;

		//stop initializing
		is_initialized_ = true;
		cout << "Initialized\n";
		//no past measurements yet, cannot predict
		return;
	}


	// ***** Prediction ***** //

	cout << "Prediction start\n";
	//time diff
	double dt = (meas_package.timestamp_ - time_us_) / 1000000.0;
	//update time
	time_us_ = meas_package.timestamp_;
	//predict
	Prediction(dt);
	cout << "Predicted\n";
	// set new time
	

	// ***** RADAR Update ***** //
	if (meas_package.sensor_type_ == MeasurementPackage::RADAR) {
		cout << "UpdateRadar\n";
		UpdateRadar(meas_package);
		cout << "Radar updated\n";
	}
	else {
		cout << "UpdateLidar\n";
		UpdateLidar(meas_package);
		cout << "Lidar updated\n";
	}
}

/**
 * Predicts sigma points, the state, and the state covariance matrix.
 * @param {double} delta_t the change in time (in seconds) between the last
 * measurement and this one.
 */
void UKF::Prediction(double delta_t) {
  /**
  TODO:

  Complete this function! Estimate the object's location. Modify the state
  vector, x_. Predict sigma points, the state, and the state covariance matrix.
  */
  //predict sigma points

	
	//create augmented mean vector
	VectorXd x_aug = VectorXd(n_aug_);
	x_aug << x_, 0, 0;

	//create augmented covariance matrix
	MatrixXd P_aug = MatrixXd(n_aug_, n_aug_);
	P_aug.fill(0.0);
	P_aug.topLeftCorner(n_x_, n_x_) = P_;
	P_aug.bottomRightCorner(2, 2) << std_a_*std_a_,0,
									 0,std_yawdd_*std_yawdd_;
	
	//create square root matrix
	MatrixXd A = P_aug.llt().matrixL();
	
	//create augmented sigma points
	MatrixXd Xsig_aug = MatrixXd(n_aug_, n_a_);
	Xsig_aug.col(0) << x_aug;

	//calc saver
	double sq = sqrt(lambda_ + n_aug_);
	//fill augmented matrix
	for (int i = 0; i< n_aug_; i++)
	{
		Xsig_aug.col(i + 1) = x_aug + sq * A.col(i);
		Xsig_aug.col(i + 1 + n_aug_) = x_aug - sq * A.col(i);
	}


	//predict sigma points
	for (int i = 0; i < n_a_; i++)
	{
		//extract values for better readability
		double p_x = Xsig_aug(0, i);
		double p_y = Xsig_aug(1, i);
		double v = Xsig_aug(2, i);
		double yaw = Xsig_aug(3, i);
		double yawd = Xsig_aug(4, i);
		double nu_a = Xsig_aug(5, i);
		double nu_yawdd = Xsig_aug(6, i);

		//predicted state values
		double px_p, py_p;

		//avoid division by zero
		if (fabs(yawd) > 0.001) {
			px_p = p_x + v / yawd * (sin(yaw + yawd*delta_t) - sin(yaw));
			py_p = p_y + v / yawd * (cos(yaw) - cos(yaw + yawd*delta_t));
		}
		else {
			px_p = p_x + v*delta_t*cos(yaw);
			py_p = p_y + v*delta_t*sin(yaw);
		}

		double v_p = v;
		double yaw_p = yaw + yawd*delta_t;
		double yawd_p = yawd;

		//add noise
		px_p = px_p + 0.5*nu_a*delta_t*delta_t * cos(yaw);
		py_p = py_p + 0.5*nu_a*delta_t*delta_t * sin(yaw);
		v_p = v_p + nu_a*delta_t;

		yaw_p = yaw_p + 0.5*nu_yawdd*delta_t*delta_t;
		yawd_p = yawd_p + nu_yawdd*delta_t;

		//write predicted sigma point into right column
		Xsig_pred_(0, i) = px_p;
		Xsig_pred_(1, i) = py_p;
		Xsig_pred_(2, i) = v_p;
		Xsig_pred_(3, i) = yaw_p;
		Xsig_pred_(4, i) = yawd_p;
	}



	x_.fill(0.0);
	for (int i = 0; i < 2 * n_aug_ + 1; i++) {
		//iterate over sigma points
		x_ += weights_(i) * Xsig_pred_.col(i);
	}

	//cout << "Xsig_pred_\n" << Xsig_pred << endl;
	//cout << "weights_\n" << weights_ << endl;
	//cout << "Xsig_pred_ * weights_\n" << Xsig_pred * weights_ << endl;
	//x_ = Xsig_pred_ * weights_;
	
	//predicted state covariance matrix
	P_.fill(0.0);
	double Two_M_PI = 2. * M_PI;

	for (int i = 0; i < 2 * n_aug_ + 1; i++) {
		// state difference
		VectorXd x_diff = Xsig_pred_.col(i) - x_;
		//angle normalization
		while (x_diff(3)> M_PI) x_diff(3) -= Two_M_PI;
		while (x_diff(3)<-M_PI) x_diff(3) += Two_M_PI;

		P_ += weights_(i) * x_diff * x_diff.transpose();
	}
	
}

/**
 * Updates the state and the state covariance matrix using a laser measurement.
 * @param {MeasurementPackage} meas_package
 */
void UKF::UpdateLidar(MeasurementPackage meas_package) {
	//set measurement dimension
	int n_z_ = 2;

	//sigma points matrix
	MatrixXd Zsig = MatrixXd(n_z_, n_a_);
	Zsig.fill(0.0);
	for (int i = 0; i < n_a_; i++)
	{
		Zsig.col(i) << Xsig_pred_(0, i), Xsig_pred_(1, i);
	}

	//mean predicted measurement
	VectorXd z_pred = VectorXd(n_z_);
	z_pred.fill(0.0);
	for (int i = 0; i < n_a_; i++)
	{
		z_pred += weights_(i) * Zsig.col(i);
	}

	//measurement covariance matrix S
	MatrixXd S_ = MatrixXd(n_z_, n_z_);
	S_.fill(0.0);
	for (int i = 0; i<2 * n_aug_ + 1; i++)
	{
		VectorXd z_diff = Zsig.col(i) - z_pred;
		S_ += weights_(i) * z_diff * z_diff.transpose();
	}

	//add measurement noise covariance matrix
	S_ += R_laser_;

	//cross correlation matrix
	MatrixXd Tc = MatrixXd(n_x_, n_z_);
	Tc.fill(0);
	for (int i = 0; i< n_a_; i++)
	{
		VectorXd x_diff = Xsig_pred_.col(i) - x_;
		VectorXd z_diff = Zsig.col(i) - z_pred;
		Tc += weights_(i) * x_diff * z_diff.transpose();
	}

	//calculate Kalman gain K
	MatrixXd K = Tc * S_.inverse();

	//update state mean and covariance matrix
	VectorXd z_diff_ = meas_package.raw_measurements_ - z_pred;
	x_ += K * z_diff_;
	P_ -= K * S_ * K.transpose();

	//calculate NIS
	NIS_laser_ = z_diff_.transpose() * S_.inverse() * z_diff_;

	//cout << "NIS_laser_ - " << NIS_laser_ << endl;
}

/**
* Updates the state and the state covariance matrix using a radar measurement.
* @param {MeasurementPackage} meas_package
*/
void UKF::UpdateRadar(MeasurementPackage meas_package) {
	//set measurement dimension, radar can measure r, phi, and r_dot
	int n_z_ = 3;
	
	//sigma points
	MatrixXd Zsig = MatrixXd(n_z_, 2 * n_aug_ + 1);
	for (int i = 0; i<2 * n_aug_ + 1; i++)
	{
		double p_x = Xsig_pred_(0, i);
		double p_y = Xsig_pred_(1, i);
		double v = Xsig_pred_(2, i);
		double yaw = Xsig_pred_(3, i);
		double rho = sqrt((p_x*p_x) + (p_y*p_y));

		//check for division by 0
		if ((fabs(p_x) < 0.001) && (fabs(p_y) < 0.001))
		{
			if (fabs(p_x) < 0.001)
			{
				p_x = 0.001;
			}
			else { p_y = 0.001; }
		}
		double phi = atan2(p_y, p_x);

		if (fabs(rho) < 0.001)
		{
			rho = 0.001;
		}
		double rho_dot = (p_x * cos(yaw) * v + p_y * sin(yaw) * v) / rho;

		Zsig(0, i) = rho;
		Zsig(1, i) = phi;
		Zsig(2, i) = rho_dot;
	}

	//mean predicted measurement
	VectorXd z_pred = VectorXd(n_z_);
	z_pred.fill(0.0);
	for (int i = 0; i<2 * n_aug_ + 1; i++){
		z_pred += weights_(i) * Zsig.col(i);
	}

	//measurement covariance matrix S
	MatrixXd S_ = MatrixXd(n_z_, n_z_);
	S_.fill(0.0);
	for (int i = 0; i < n_a_; i++){
		VectorXd z_diff = Zsig.col(i) - z_pred;

		//angle normalization
		while (z_diff(1)> M_PI) z_diff(1) -= 2.*M_PI;
		while (z_diff(1)<-M_PI) z_diff(1) += 2.*M_PI;

		S_ += weights_(i) * z_diff * z_diff.transpose();
	}

	//add measurement noise covariance matrix
	S_ += R_radar_;

	//cross correlation matrix
	MatrixXd Tc = MatrixXd(n_x_, n_z_);
	Tc.fill(0.0);
	for (int i = 0; i < n_a_; i++)
	{
		VectorXd x_diff = Xsig_pred_.col(i) - x_;

		//normalize
		while (x_diff(3)> M_PI) x_diff(3) -= 2.*M_PI;
		while (x_diff(3)<-M_PI) x_diff(3) += 2.*M_PI;

		VectorXd z_diff = Zsig.col(i) - z_pred;

		while (z_diff(1)> M_PI) z_diff(1) -= 2.*M_PI;
		while (z_diff(1)<-M_PI) z_diff(1) += 2.*M_PI;

		Tc += weights_(i) * x_diff * z_diff.transpose();
	}

	//Kalman gain
	MatrixXd K = Tc * S_.inverse();
	//difference with truth
	VectorXd z_diff = meas_package.raw_measurements_ - z_pred;

	while (z_diff(1)> M_PI) z_diff(1) -= 2.*M_PI;
	while (z_diff(1)<-M_PI) z_diff(1) += 2.*M_PI;

	//update state mean and covariance matrix
	x_ += K * z_diff;
	P_ -= K * S_ * K.transpose();

	//calculate NIS
	NIS_radar_ = z_diff.transpose() * S_.inverse() * z_diff;

	//cout << "NIS_radar_ - " << NIS_radar_ << endl;
}